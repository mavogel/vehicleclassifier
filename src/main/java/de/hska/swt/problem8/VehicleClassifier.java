/**
 *
 */
package de.hska.swt.problem8;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.semanticweb.HermiT.Reasoner.ReasonerFactory;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.AddAxiom;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLAnnotation;
import org.semanticweb.owlapi.model.OWLAnnotationObjectVisitorEx;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLClassAssertionAxiom;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLDataProperty;
import org.semanticweb.owlapi.model.OWLDataPropertyAssertionAxiom;
import org.semanticweb.owlapi.model.OWLEntity;
import org.semanticweb.owlapi.model.OWLException;
import org.semanticweb.owlapi.model.OWLLiteral;
import org.semanticweb.owlapi.model.OWLNamedIndividual;
import org.semanticweb.owlapi.model.OWLObjectProperty;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.model.OWLOntologyStorageException;
import org.semanticweb.owlapi.reasoner.InferenceType;
import org.semanticweb.owlapi.reasoner.NodeSet;
import org.semanticweb.owlapi.reasoner.OWLReasoner;
import org.semanticweb.owlapi.reasoner.structural.StructuralReasonerFactory;
import org.semanticweb.owlapi.util.InferredAxiomGenerator;
import org.semanticweb.owlapi.util.InferredOntologyGenerator;
import org.semanticweb.owlapi.util.InferredSubClassAxiomGenerator;
import org.semanticweb.owlapi.util.OWLObjectVisitorExAdapter;

import uk.ac.manchester.cs.owl.owlapi.OWLClassImpl;

/**
 * Helper class to extract a label from an {@link OWLClass}
 *
 * @author Ignazio Palmisano
 *
 */
class LabelExtractor extends OWLObjectVisitorExAdapter<String> implements OWLAnnotationObjectVisitorEx<String> {
    public LabelExtractor() {
        super("defaultReturnValue");
    }

    @Override
    public String visit(final OWLAnnotation annotation) {
        if (annotation.getProperty().isLabel()) {
            OWLLiteral c = (OWLLiteral) annotation.getValue();
            return c.getLiteral();
        }
        return null;
    }
}

/**
 * Starter for a vehicle classifier
 *
 * @author mvogel
 *
 */
public class VehicleClassifier {

    // cmd line options
    static final String OPTION_NAME = "name";
    static final String OPTION_AXLES = "axles";
    static final String OPTION_ENGINE = "engine";
    static final String OPTION_CHAIN = "chain";
    static final String OPTION_FULL_LENGTH = "full-length";
    static final String OPTION_LOAD = "load";
    static final String OPTION_SEATS = "seats";
    static final String OPTION_TIRES = "tires";
    static final String OPTION_TRAILER = "trailers";

    // owl constants
    static final String OWL_FILE_NAME_BASENAME = "task8";
    static final String OWL_FILE_NAME_TYPE = ".owl";
    static final String OWL_FILE_NAME = OWL_FILE_NAME_BASENAME + OWL_FILE_NAME_TYPE;

    static final String TASK_8_URL = "http://www.semanticweb.org/mvogel/ontologies/2015/11/vehicles-ontology";
    static final String BASE_RESOURCE_DIR = System.getProperty("user.dir") + "/src/main/resources/";
    static final String TASK_8_FILE_PATH = VehicleClassifier.BASE_RESOURCE_DIR + VehicleClassifier.OWL_FILE_NAME;
    static final IRI TASK_8_IRI = IRI.create(TASK_8_FILE_PATH);

    // member
    private final OWLOntologyManager m = OWLManager.createOWLOntologyManager();
    private final OWLDataFactory df = OWLManager.getOWLDataFactory();
    private final LabelExtractor le = new LabelExtractor();
    private OWLReasoner reasoner;
    private OWLOntology on;

    public OWLOntology getOntology() {
        return this.on;
    }

    /**
     * Extracts all 'rdf' labels for an entity
     *
     * @param clazz the {@link OWLEntity}
     * @param o the {@link OWLOntology}
     * @return the label as String
     */
    private String labelFor(final OWLEntity clazz, final OWLOntology o) {
        // Set<OWLAnnotationProperty> annotations = clazz.getAnnotationPropertiesInSignature();
        // for (OWLAnnotationProperty anno : annotations) {
        // String result = anno.accept(this.le);
        // if (result != null) {
        // return result;
        // }
        // }
        // return clazz.getIRI().toString(); OLD

        Set<OWLDataProperty> dataProps = clazz.getDataPropertiesInSignature();
        for (OWLDataProperty dataProp : dataProps) {
            String result = dataProp.accept(this.le);
            if (result != null) {
                return result;
            }
        }

        Set<OWLObjectProperty> objectProps = clazz.getObjectPropertiesInSignature();
        for (OWLObjectProperty objectProp : objectProps) {
            String result = objectProp.accept(this.le);
            if (result != null) {
                return result;
            }
        }


        return clazz.getIRI().toString();

    }

    /**
     * Loads the file from the classloader
     *
     * @param name the name of the file
     * @return the {@link InputStream}
     */
    InputStream loadOwlFile(final String name) {
        return VehicleClassifier.class.getClassLoader().getResourceAsStream(name);
    }

    /**
     * Loads the ontology from {@link VehicleClassifier#OWL_FILE_NAME}
     *
     * @return the {@link OWLOntology}
     * @throws RuntimeException if the ontology could not be loaded
     */
    OWLOntology loadOntology() {
        try {
            return this.m.loadOntologyFromOntologyDocument(loadOwlFile(VehicleClassifier.OWL_FILE_NAME));
        } catch (OWLOntologyCreationException e) {
            System.err.println(e.getMessage());
            throw new RuntimeException("could not load ontology from file '" + VehicleClassifier.OWL_FILE_NAME + "'");
        }

    }

    /**
     * Prints the hierarchy of the owl file
     *
     * @param reasoner
     * @param clazz
     * @param level
     * @param visited
     * @throws OWLException
     */
    private void printHierarchy(final OWLReasoner reasoner, final OWLClass clazz, final int level,
            final Set<OWLClass> visited) throws OWLException {
        // Only print satisfiable classes to skip Nothing
        if (!visited.contains(clazz) && reasoner.isSatisfiable(clazz)) {
            visited.add(clazz);
            for (int i = 0; i < (level * 4); i++) {
                System.out.print(" ");
            }
            System.out.println(labelFor(clazz, reasoner.getRootOntology()));
            // Find the children and recurse
            NodeSet<OWLClass> classes = reasoner.getSubClasses(clazz, true);
            for (OWLClass child : classes.getFlattened()) {
                printHierarchy(reasoner, child, level + 1, visited);
            }
        }
    }

    /**
     * Prints the hierarchy of the owl file
     */
    void printHierarchy() {
        if (this.reasoner == null) {
            throw new RuntimeException("reasoner not initialized. bootstrap needed!");
        }

        OWLDataFactory df = OWLManager.getOWLDataFactory();
        OWLClass clazz = df.getOWLThing();
        System.out.println("Class : " + clazz);
        try {
            printHierarchy(this.reasoner, clazz, 0, new HashSet<>());
        } catch (OWLException e) {
            e.printStackTrace();
            throw new RuntimeException(e.getMessage());
        }
    }

    /**
     * Prints all instances of the classes after the reasoner pre-computed the inferences
     */
    void printInstancesOfClasses(final OWLOntology on) {
        if (this.reasoner == null) {
            throw new RuntimeException("reasoner not initialized. bootstrap needed!");
        }

        // reasoner from previous example...
        // for each class, look up the instances
        for (OWLClass c : on.getClassesInSignature()) {
            // the boolean argument specifies direct subclasses
            for (OWLNamedIndividual i : this.reasoner.getInstances(c, true).getFlattened()) {
                System.out.println(labelFor(i, on) + ":" + labelFor(c, on)); // look up all property
                                                                             // assertions
                // object properties
                for (OWLObjectProperty op : on.getObjectPropertiesInSignature()) {
                    NodeSet<OWLNamedIndividual> petValuesNodeSet = this.reasoner.getObjectPropertyValues(i, op);
                    for (OWLNamedIndividual value : petValuesNodeSet.getFlattened()) {
                        System.out.println(labelFor(i, on) + " | " + labelFor(op, on) + " | " + labelFor(value, on));
                    }
                }

                // data properties
                Set<OWLDataPropertyAssertionAxiom> dataProperties = this.on.getDataPropertyAssertionAxioms(i);
                for (OWLDataPropertyAssertionAxiom ax : dataProperties) {
                    System.out.println("\t" + ax.getProperty() + ":" + ax.getObject());
                }
            }
        }
    }

    /**
     * Retrieves the name of the class an individual belongs to
     *
     * @param nameOfTheIndividual the name of the individual
     * @return the name of the class
     */
    String getClassForIndividual(final String nameOfTheIndividual, final OWLOntology on) {
        if (this.reasoner == null) {
            throw new RuntimeException("reasoner not initialized. bootstrap needed!");
        }

        // reasoner from previous example...
        // for each class, look up the instances
        for (OWLClass c : on.getClassesInSignature()) {
            // the boolean argument specifies direct subclasses
            for (OWLNamedIndividual i : this.reasoner.getInstances(c, true).getFlattened()) {
                if (nameOfTheIndividual.equals(i.getIRI().getShortForm())) {
                    return c.getIRI().getShortForm();
                }
            }
        }
        return "NOT_CLASSIFIED";
    }

    /**
     * Performs all bootstrap operations
     */
    void bootstrap() {
        this.on = loadOntology();
        this.reasoner = new StructuralReasonerFactory().createReasoner(this.on);
        precomputeInferences();
    }

    /**
     * Computes the inferences by the reasoner
     */
    void precomputeInferences() {
        if (this.reasoner == null) {
            throw new RuntimeException("reasoner not initialized. bootstrap needed!");
        }
        this.reasoner.precomputeInferences(InferenceType.CLASS_HIERARCHY);
    }

    /**
     * Computes the inferred axioms and classifies the individuals on the given ontology
     *
     * @param newOntology the new ontology
     *
     */
    OWLOntology computeInferedAxioms(final OWLOntology newOntology) {
        if (this.reasoner == null) {
            throw new RuntimeException("reasoner not initialized. bootstrap needed!");
        }

        List<InferredAxiomGenerator<? extends OWLAxiom>> gens =
                new ArrayList<InferredAxiomGenerator<? extends OWLAxiom>>();
        gens.add(new InferredSubClassAxiomGenerator());

        OWLOntology infOnt;
        try {
            infOnt = this.m.createOntology();
        } catch (OWLOntologyCreationException e) {
            e.printStackTrace();
            throw new RuntimeException(e.getMessage());
        }

        this.reasoner = new ReasonerFactory().createNonBufferingReasoner(newOntology);

        InferredOntologyGenerator iog = new InferredOntologyGenerator(this.reasoner, gens);
        iog.fillOntology(this.m, infOnt);
        // iog.fillOntology(this.m.getOWLDataFactory(), infOnt);

        InputStream inStream;
        ByteArrayOutputStream outStream;
        try {
            outStream = new ByteArrayOutputStream();
            this.m.saveOntology(infOnt, outStream);
            inStream = new ByteArrayInputStream(outStream.toByteArray());
        } catch (OWLOntologyStorageException e) {
            e.printStackTrace();
            throw new RuntimeException(e.getMessage());
        }

        try {
            return this.m.loadOntologyFromOntologyDocument(inStream);
        } catch (OWLOntologyCreationException e) {
            e.printStackTrace();
            throw new RuntimeException(e.getMessage());
        }
    }

    /**
     * Creates an object property axiom which can be added to the ontology
     *
     * @param individualToAddTo the individual to add the object property to
     * @param owlObjectProperty the object property
     * @param owlNamedIndivdual the individual to add as object property
     * @return the {@link AddAxiom}
     */
    private AddAxiom createObjectPropertyAddAxiom(final OWLNamedIndividual individualToAddTo,
            final String owlObjectProperty, final String owlNamedIndivdual) {
        return new AddAxiom(this.on,
                this.df.getOWLObjectPropertyAssertionAxiom(//
                        this.df.getOWLObjectProperty(IRI.create(TASK_8_URL + "#" + owlObjectProperty)), //
                        individualToAddTo, //
                        this.df.getOWLNamedIndividual(IRI.create(TASK_8_URL + "#" + owlNamedIndivdual))));
    }

    /**
     * Add the Vehicle to the ontology and classifies it
     *
     * @param cmd the command line holding the parameters of the vehicle
     */
    String addVehicleAndClassify(final CommandLine cmd) {
        OWLOntology inferredOntology;
        // == step 1: create and add
        OWLNamedIndividual toClassify =
                this.df.getOWLNamedIndividual(IRI.create(TASK_8_URL + "#" + cmd.getOptionValue(OPTION_NAME)));
        OWLClassExpression classExpression = new OWLClassImpl(IRI.create(TASK_8_URL + "#Vehicle"));
        OWLClassAssertionAxiom addToVehicleClassAxiom = this.df.getOWLClassAssertionAxiom(classExpression, toClassify);

        List<AddAxiom> axiomList = new ArrayList<>();
        if (Boolean.valueOf(cmd.getOptionValue(OPTION_ENGINE))) {
            axiomList.add(createObjectPropertyAddAxiom(toClassify, "hasPart", "Engine"));
        }
        if (Boolean.valueOf(cmd.getOptionValue(OPTION_CHAIN))) {
            axiomList.add(createObjectPropertyAddAxiom(toClassify, "hasPart", "Chain"));
        }

        OWLDataProperty seats = this.df.getOWLDataProperty(IRI.create(TASK_8_URL + "#seats"));
        OWLDataProperty axles = this.df.getOWLDataProperty(IRI.create(TASK_8_URL + "#axles"));
        OWLDataProperty fullLength = this.df.getOWLDataProperty(IRI.create(TASK_8_URL + "#fullLength"));
        OWLDataProperty load = this.df.getOWLDataProperty(IRI.create(TASK_8_URL + "#load"));
        OWLDataProperty tires = this.df.getOWLDataProperty(IRI.create(TASK_8_URL + "#tires"));
        OWLDataProperty trailers = this.df.getOWLDataProperty(IRI.create(TASK_8_URL + "#trailers"));

        OWLDataPropertyAssertionAxiom seatsAxiom = this.df.getOWLDataPropertyAssertionAxiom(seats, toClassify,
                Integer.valueOf(cmd.getOptionValue(OPTION_SEATS)));
        OWLDataPropertyAssertionAxiom axlesAxiom = this.df.getOWLDataPropertyAssertionAxiom(axles, toClassify,
                Integer.valueOf(cmd.getOptionValue(OPTION_AXLES)));
        OWLDataPropertyAssertionAxiom fullLengthAxiom = this.df.getOWLDataPropertyAssertionAxiom(fullLength, toClassify,
                Boolean.valueOf(cmd.getOptionValue(OPTION_FULL_LENGTH)));
        OWLDataPropertyAssertionAxiom loadAxiom = this.df.getOWLDataPropertyAssertionAxiom(load, toClassify,
                Boolean.valueOf(cmd.getOptionValue(OPTION_LOAD)));
        OWLDataPropertyAssertionAxiom tiresAxiom = this.df.getOWLDataPropertyAssertionAxiom(tires, toClassify,
                Integer.valueOf(cmd.getOptionValue(OPTION_TIRES)));
        OWLDataPropertyAssertionAxiom trailerAxiom = this.df.getOWLDataPropertyAssertionAxiom(trailers, toClassify,
                Integer.valueOf(cmd.getOptionValue(OPTION_TRAILER)));

        axiomList.addAll(Arrays.asList(new AddAxiom(this.on, addToVehicleClassAxiom), new AddAxiom(this.on, seatsAxiom),
                new AddAxiom(this.on, axlesAxiom), new AddAxiom(this.on, fullLengthAxiom),
                new AddAxiom(this.on, loadAxiom), new AddAxiom(this.on, tiresAxiom),
                new AddAxiom(this.on, trailerAxiom)));
        this.m.applyChanges(axiomList);

        // == step 2: save to local modified ontology
        InputStream inStream;
        ByteArrayOutputStream outStream;
        try {
            outStream = new ByteArrayOutputStream();
            this.m.saveOntology(this.on, outStream);
            inStream = new ByteArrayInputStream(outStream.toByteArray());
            this.m.removeOntology(this.on);
            inferredOntology = this.m.loadOntologyFromOntologyDocument(inStream);
        } catch (OWLOntologyStorageException | OWLOntologyCreationException e) {
            e.printStackTrace();
            throw new RuntimeException(e.getMessage());
        }

        // == step 3: run the reasoner
        inferredOntology = computeInferedAxioms(inferredOntology);

        // == step 4: extract the class the new individuals belong to and print it
        return getClassForIndividual(cmd.getOptionValue(OPTION_NAME), inferredOntology);
    }

    /**
     * Runs the tool
     *
     * @param args the program arguments
     */
    void run(final String[] args) {
        // == bootstrap
        CommandLine cmd = initCmdWithArgs(args);
        bootstrap();

        // == go and classify
        System.out.println(addVehicleAndClassify(cmd));
    }

    /**
     * Initializes the required command line arguments and parses them against the given ones
     *
     * @param args the args from the command line
     * @return the command line if the args were parsed successfully
     * @throws RuntimeException if the args were not correct
     */
    CommandLine initCmdWithArgs(final String[] args) {
        Options options = new Options();
        // options...
        //@formatter:off
        options.addOption(Option.builder("n").longOpt(OPTION_NAME).required().hasArg()
                .desc("the name of the vehicle to classify").type(String.class).build());
        options.addOption(Option.builder("a").longOpt(OPTION_AXLES).required().hasArg()
                .desc("the number of axles").type(Integer.class).build());
        options.addOption(Option.builder("s").longOpt(OPTION_SEATS).required().hasArg()
                .desc("the number of seats").type(Integer.class).build());
        options.addOption(Option.builder("ti").longOpt(OPTION_TIRES).required().hasArg()
                .desc("the number of tires").type(Integer.class).build());
        options.addOption(Option.builder("tr").longOpt(OPTION_TRAILER).required().hasArg()
                .desc("the number of trailers").type(Integer.class).build());
        options.addOption(Option.builder("c").longOpt(OPTION_CHAIN).required().hasArg()
                .desc("true if the vehicle has a chain, false otherwise").type(Boolean.class).build());
        options.addOption(Option.builder("e").longOpt(OPTION_ENGINE).required().hasArg()
                .desc("true if the vehicle has an engine, false otherwise").type(Boolean.class).build());
        options.addOption(Option.builder("fl").longOpt(OPTION_FULL_LENGTH).required().hasArg()
                .desc("true if the vehicle has full length, false otherwise").type(Boolean.class).build());
        options.addOption(Option.builder("l").longOpt(OPTION_LOAD).required().hasArg()
                .desc("true if the vehicle can take a load, false otherwise").type(Boolean.class).build());
        //@formatter:on

        CommandLineParser parser = new DefaultParser();
        CommandLine parsedResult;
        try {
            parsedResult = parser.parse(options, args);
        } catch (ParseException e) {
            usage(options);
            throw new RuntimeException(e.getMessage());
        }

        // TODO checks for correct types
        // for (Option o : parsedResult.getOptions()) {
        // try {
        // System.out.println(o.getValue() + ": " + o.getType().getClass());
        // o.getType().getClass().cast(o.getValue());
        // } catch (ClassCastException cce) {
        // throw new RuntimeException(
        // o.getLongOpt() + " is not of type " + o.getType().getClass().getSimpleName());
        // }
        // }

        return parsedResult;
    }

    /**
     * Prints the usage of the tool.
     *
     * @param options the options
     */
    void usage(final Options options) {
        new HelpFormatter().printHelp("VehicleClassifier [OPTION]", "Classifies a given vehicle", options, "", false);
    }

    /**
     * Starts the tool
     *
     * @param args the program arguments
     */
    public static void main(final String[] args) {
        try {
            new VehicleClassifier().run(args);
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }
}

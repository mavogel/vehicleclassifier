/**
 *
 */
package de.hska.swt.problem8;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import org.apache.commons.cli.CommandLine;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.semanticweb.owlapi.model.OWLOntology;

/**
 * Tests for {@link VehicleClassifier}
 *
 * @author mvogel
 *
 */
public class VehicleClassifierTest {

    private VehicleClassifier classUnderTest;

    @Before
    public void setUp() {
        this.classUnderTest = new VehicleClassifier();
    }

    @Test
    public void shouldLoadTheOWLDocumentFromResources() throws Exception {
        assertNotNull(this.classUnderTest.loadOwlFile(VehicleClassifier.OWL_FILE_NAME));
    }

    @Test
    public void shouldSuccessfullyLoadOntology() throws Exception {
        assertNotNull(this.classUnderTest.loadOntology());
    }

    @Test
    public void shouldParseCmdOptions() throws Exception {
        assertNotNull(this.classUnderTest.initCmdWithArgs(new String[] { "-n Smart", "-a33", "-s3", "-ti", "4", "-tr",
                "1", "-e", "true", "-c", "true", "-fl", "true", "-l", "true" }));
    }

    @Test
    public void shouldParseLongCmdOptions() throws Exception {
        assertNotNull(this.classUnderTest.initCmdWithArgs(
                new String[] { "--name", "Smart", "--axles", "33", "--seats", "3", "--tires", "4", "--trailers", "1",
                        "--engine", "true", "--chain", "true", "--full-length", "true", "--load", "true" }));
    }

    @Test
    @Ignore("has to be implemented")
    public void shouldFailToParseCmdOptionsDueToTypeMismatchForEngine() throws Exception {
        assertNotNull(this.classUnderTest.initCmdWithArgs(
                new String[] { "--name", "Smart", "--axles", "33", "--seats", "3", "--tires", "4", "--trailers", "1",
                        "--engine", "abc", "--chain", "true", "--full-length", "true", "--load", "true" }));
    }

    @Test(expected = RuntimeException.class)
    public void shouldPrintUsageOptions() throws Exception {
        assertNull(this.classUnderTest.initCmdWithArgs(new String[] { "-zz 3" }));
    }

    @Test
    public void shouldPrintTheClassHierarchy() throws Exception {
        this.classUnderTest.bootstrap();
        this.classUnderTest.printHierarchy();
    }

    @Test
    public void shouldPrintAllInstancesOfTheClasses() throws Exception {
        this.classUnderTest.bootstrap();
        this.classUnderTest.printInstancesOfClasses(this.classUnderTest.getOntology());
    }

    @Test
    public void shouldFindSchoolBusInBusClass() throws Exception {
        this.classUnderTest.bootstrap();
        OWLOntology ontologyWithInferedAxioms =
                this.classUnderTest.computeInferedAxioms(this.classUnderTest.getOntology());
        assertEquals("Bus", this.classUnderTest.getClassForIndividual("SchoolBus", ontologyWithInferedAxioms));
    }

    @Test
    public void shouldAddAmericanSchoolBusVehicleToOntologyAndClassifyItAsBus() throws Exception {
        this.classUnderTest.bootstrap();
        CommandLine cmd = this.classUnderTest.initCmdWithArgs(new String[] { "--name", "AmericanSchoolBus", "--axles",
                "2", "--seats", "22", "--tires", "4", "--trailers", "0", "--engine", "true", "--chain", "false",
                "--full-length", "true", "--load", "false" });
        assertEquals("Bus", this.classUnderTest.addVehicleAndClassify(cmd));
    }

    @Test
    public void shouldAddKawasakiToOntologyAndClassifyItAsMotorcycle() throws Exception {
        this.classUnderTest.bootstrap();
        CommandLine cmd = this.classUnderTest.initCmdWithArgs(
                new String[] { "--name", "Kawasaki", "--axles", "2", "--seats", "1", "--tires", "2", "--trailers", "0",
                        "--engine", "true", "--chain", "true", "--full-length", "false", "--load", "false" });
        assertEquals("Motorcycle", this.classUnderTest.addVehicleAndClassify(cmd));
    }

    @Test
    public void shouldAddMercedesToOntologyAndClassifyItAsCar() throws Exception {
        this.classUnderTest.bootstrap();
        CommandLine cmd = this.classUnderTest.initCmdWithArgs(
                new String[] { "--name", "Mercedes", "--axles", "2", "--seats", "2", "--tires", "4", "--trailers", "0",
                        "--engine", "true", "--chain", "false", "--full-length", "false", "--load", "true" });
        assertEquals("Car", this.classUnderTest.addVehicleAndClassify(cmd));
    }

    @Test
    public void shouldAddVigoVanToOntologyAndClassifyItAsVan() throws Exception {
        this.classUnderTest.bootstrap();
        CommandLine cmd = this.classUnderTest.initCmdWithArgs(
                new String[] { "--name", "VigoVan", "--axles", "2", "--seats", "6", "--tires", "4", "--trailers", "0",
                        "--engine", "true", "--chain", "false", "--full-length", "false", "--load", "true" });
        assertEquals("Van", this.classUnderTest.addVehicleAndClassify(cmd));
    }

    @Test
    public void shouldAddBigTrailerTruckVanToOntologyAndClassifyItAsTrailerTruck() throws Exception {
        this.classUnderTest.bootstrap();
        CommandLine cmd = this.classUnderTest.initCmdWithArgs(new String[] { "--name", "BigTrailerTruck", "--axles",
                "3", "--seats", "2", "--tires", "12", "--trailers", "2", "--engine", "true", "--chain", "false",
                "--full-length", "false", "--load", "true" });
        assertEquals("TrailerTruck", this.classUnderTest.addVehicleAndClassify(cmd));
    }
}

# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* The program uses the OWL API 3.5.1 and the Hermit Reasoner to classify vehicles concerning their attributes.
* Version 1.0

### How do I get set up? ###

* install maven and java8
* build: mvn clean install
* example call: 

```
#!bash
java -jar target/VehicleClassifier-jar-with-dependencies.jar --name Honda --axles 2 --seats 1 --tires 3 --trailers 0 --engine true --chain true --full-length false --load false
```


### Who do I talk to? ###

* The repo owner